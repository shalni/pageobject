package week1.day2;

import java.util.Scanner;

public class Prime {
	public static void main(String[] args) {
		int prime;
		int t=0;
		Scanner scan= new Scanner(System.in);
		System.out.println("Enter the prime number");
		prime=scan.nextInt();
		scan.close();
		for(int j=prime-1;j>1;j--)
		{if(prime%j==0)	
		 {t=1;
		  break;
		 }
		}
		if(t==0)
			System.out.println(prime+" is a Prime Number");
		else
			System.out.println(prime+" is not a Prime Number");
	}

}
