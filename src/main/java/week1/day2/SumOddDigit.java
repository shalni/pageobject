package week1.day2;

import java.util.Scanner;

public class SumOddDigit {
public static void main(String[] args) {
	Scanner scan=new Scanner(System.in);
	int number;
	int s=0;
	int t;
	System.out.println("Enter a number");
	number=scan.nextInt();
	scan.close();
    while(number!=0)
	{t=number%10;
	 if(t%2!=0)
		 s=s+t;
	 number=number/10;
	}
    System.out.println("Sum of Odd Numbers:" + s);
 }
}
