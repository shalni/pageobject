package week1.day2;

import java.util.Scanner;

public class OddSum {

	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		int size;
		int sum=0;
		System.out.println("Enter the size of the Array");
		size=scan.nextInt();
		int[]numbers=new int[size];
		System.out.println("Enter the "+size+" numbers");
		for (int i=0;i<size;i++)
		numbers[i]=scan.nextInt();
		scan.close();
		for(int eachNum:numbers)
		{if(eachNum%2!=0)
			sum=sum+eachNum;
		}
		
	System.out.println("Sum of the odd numbers: "+ sum);

	}

}
