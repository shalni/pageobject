package week1.day2;

import java.util.Scanner;

public class SumPrimeArray {
public static void main(String[] args) {
	Scanner scan=new Scanner(System.in);
	int size;
	System.out.println("Enter the size of the Array");
	size=scan.nextInt();
	int[]arrayPrime=new int[size];
	int sum=0;
	int t=0;
	System.out.println("Enter the "+size+" numbers");
	for(int i=0;i<size;i++)
	{arrayPrime[i]=scan.nextInt();
	 for(int j=arrayPrime[i]-1;j>1;j--)
	 {if(arrayPrime[i]%j==0)	
	  {t=1;
	   continue;
	  }
	 }
	 if((t==0) && (arrayPrime[i]!=1))
	 sum=sum+arrayPrime[i];
	 t=0;
	 }
	scan.close();
	System.out.println("Sum of Prime Number Values in Array:"+ sum);
}
}
