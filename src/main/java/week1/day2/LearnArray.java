package week1.day2;

import java.util.Scanner;

public class LearnArray {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		Scanner scan=new Scanner(System.in);
		int size;
		System.out.println("Enter the size of the Array");
		size=scan.nextInt();
		int[]salary=new int[size];
		//Normal For Loop
		System.out.println("Enter the "+size+" salaries");
		for (int i=0;i<size;i++)
		salary[i]=scan.nextInt();
		scan.close();
		System.out.println("The Salaries are");
		//For each loop
		for (int eachSal:salary)
		System.out.print(eachSal+" ");
	}

}
