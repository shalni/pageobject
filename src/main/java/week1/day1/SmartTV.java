package week1.day1;

public class SmartTV {

	public String brand="Sony";
	public String model="LXI";
	
	public void changeChannel(int channelnum)
	{
		System.out.println("Switch to Channel " + channelnum);
	}
	
	public void changeMode(String mode)
	{
		System.out.println("Change to "+ mode);
	}
}
