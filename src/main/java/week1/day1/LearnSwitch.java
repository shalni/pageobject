package week1.day1;

import java.util.Scanner;

public class LearnSwitch {
	
	void doOperation()
	{
	Scanner scan= new Scanner(System.in);
	System.out.println("Enter first number");
	int v1=scan.nextInt();
	System.out.println("Enter second number");
	int v2=scan.nextInt();
	System.out.println("Enter the Operation");
	String opr=scan.next();
	scan.close();
	switch (opr) {
	case "add":System.out.println("Adding "+ (v1+v2));
		break;
	case "sub":System.out.println("Subtraction "+ (v1-v2));
		break;
	default:System.out.println("Invalid Operation");
		break;
	}
		
	}
	
	public static void main(String[] args) {
				LearnSwitch swt=new LearnSwitch();
				swt.doOperation();
	}

}
