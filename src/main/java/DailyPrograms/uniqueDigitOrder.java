package DailyPrograms;

import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class uniqueDigitOrder {
	
public static void main(String[] args) {
	int n;
	Scanner s= new Scanner(System.in);
	System.out.println("Enter the number ");
	n=s.nextInt();
	s.close();
	Set<Integer> number=new TreeSet<Integer>(); 
	while(n>0)
	{number.add(n%10);
	 n=n/10;
	}
	
	System.out.println("Unique Digits in Sorted Order: ");
	Iterator<Integer> itr=number.iterator();  
	  while(itr.hasNext()){  
	   System.out.print(itr.next());  
	  }  
	
	
  }
}
