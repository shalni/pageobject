package DailyPrograms;

import java.util.Scanner;

public class MatrixSum {
	public static void main(String[] args) {
		int n;
		Scanner s= new Scanner(System.in);
		System.out.println("Enter the size of the matrix ");
		n=s.nextInt();
		int[][] matrix=new int[n][n];
		System.out.println("Enter the matrix ");
		for(int i=0;i<n;i++)
		{for(int j=0;j<n;j++)
			matrix[i][j]=s.nextInt();
		}
		s.close();
		System.out.println("Printing the Matrix ");
		for(int i=0;i<n;i++)
		{for(int j=0;j<n;j++)
		 System.out.print(" "+matrix[i][j]);
		 System.out.println();
		}
		
		int sum=0;
		
		for(int i=0,j=n-1;i<n;i++,j--)
			sum=sum+matrix[i][j];
		System.out.println("Sum of Matrix: "+sum);
		
				   
		}
	}


