
//Daily Assignment - 0310, 0510

package DailyPrograms;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Assign12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","C:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leafground.com/pages/Dropdown.html");
		driver.manage().window().maximize();
		Select eleDrop= new Select(driver.findElementByXPath("//select[@id='dropdown1']"));
		List<WebElement> allOptions=eleDrop.getOptions();
		int count=allOptions.size();
		System.out.println("Last but one element " + allOptions.get(count-2).getText());
		driver.get("http://leafground.com/pages/checkbox.html");
		driver.manage().window().maximize();
		WebElement check1=driver.findElementByXPath("(//input[@type='checkbox'])[1]");
		WebElement check2=driver.findElementByXPath("(//input[@type='checkbox'])[6]");
		Assign12 a=new Assign12();
		a.checkStatus(check1);
		a.checkStatus(check2);
		
	}
	 void checkStatus(WebElement w)
	 {if(w.isSelected())
		 {System.out.println(w.getText()+ "Check Box is Selected");
		  w.click();
		  System.out.println("Unselecting");
		 }
		 
	 else
		 {System.out.println(w.getText()+ "Check Box is not Selected");
		  w.click();
		  System.out.println("Selecting");
		 }
		 
	 
	 }

}
