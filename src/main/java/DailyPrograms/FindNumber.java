package DailyPrograms;

import java.util.Scanner;

public class FindNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s= new Scanner(System.in);
		int num;
		System.out.println("Enter the number");
		num=s.nextInt();
		int n=num;
		s.close();
		int greaterDigit=0;
		int t;
		while(num!=0)
		{t=num%10;
		 if(greaterDigit < t)
			 greaterDigit=t;
		 num=num/10;
		}
		String digitStr=String.valueOf(greaterDigit);
		String str=String.valueOf(n);
		while(str.contains(digitStr))
		{n=n-1;
		 str=String.valueOf(n);
		}
		System.out.println(n);
	}
}
