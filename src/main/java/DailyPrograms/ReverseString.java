package DailyPrograms;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;



public class ReverseString {
	
	public void reverseString(Set<String> reverse, String s, int l) 
	{reverse.add(s.substring(l-1, l));
	 l=l-1;
	 if(l>=1)
	 reverseString(reverse,s,l);  		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
       String s="Testleaf";
       Set<String> reverse=new LinkedHashSet<String>();
       int l=s.length();
       ReverseString rs= new ReverseString();
       rs.reverseString(reverse,s,l );
       Iterator<String> iter = reverse.iterator();
       while (iter.hasNext()) {
           System.out.print(iter.next());
       }
 
	}
}
