package DailyPrograms;

import java.util.Scanner;

public class UniqueString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String myString;
		System.out.println("Enter the String: ");
		Scanner s= new Scanner(System.in);
		myString=s.nextLine();
		s.close();
		String uniqueString=myString.substring(0,1).toLowerCase();
		for(int i=0;i<myString.length();i++)
		{	if((uniqueString.contains(myString.substring(i, i+1).toLowerCase()))==false)
				uniqueString=uniqueString.concat(myString.substring(i, i+1).toLowerCase());
		}
		System.out.println("Unique String: "+uniqueString);
	}

}
