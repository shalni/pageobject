//0410 Daily Question - Swap two number without 3rd variable

package DailyPrograms;

public class Swap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a=10, b=30;
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("Value of a "+ a);
		System.out.println("Value of b "+ b);

	}

}
