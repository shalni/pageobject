package DailyPrograms;

import java.util.Scanner;
import java.util.regex.Pattern;

public class CharGroup {
	public static void main(String[] args) 
	{
		String inputText;
		 Scanner scan=new Scanner(System.in);
		 System.out.println("Enter the String:");
		 inputText="HELLO playground &*() 3456";
		 Pattern ucasePat=Pattern.compile("[A-Z]");
		 Pattern lcasePat=Pattern.compile("[a-z]");
		 Pattern digitPat=Pattern.compile("[0-9]");
		 int uCount=0;
		 int lCount=0;
		 int dCount=0;
		 System.out.println(inputText);
		 scan.close();
		 int len=inputText.length();
		 for (int i=0;i<len;i++)
		 {if(((ucasePat.matcher(inputText.substring(i, i+1)).matches())==true))
		   uCount++;
		 else if(((lcasePat.matcher(inputText.substring(i, i+1)).matches())==true))
		    lCount++;
		 else if(((digitPat.matcher(inputText.substring(i, i+1)).matches())==true))
			dCount++;
		 }
		System.out.println("Capital Letters "+ (uCount*100)/len);
		System.out.println("Small Letters "+ (lCount*100)/len);
		System.out.println("Numbers "+ (dCount*100)/len);
		System.out.println("Special Characters"+(len-(uCount+lCount+dCount)*100)/len);
	
	}
}
