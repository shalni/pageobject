//CC #07

package DailyPrograms;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s= new Scanner(System.in);
		System.out.println("Enter the Year: ");
		int year=s.nextInt();
		int leap=0;
		s.close();
		if(year%100==0)
		{if(year%400==0)
		 leap=1;
		}
		else if(year%4==0)
			leap=1;
		if(leap==0)
			System.out.println(year+" Not a Leap Year");
		else
			System.out.println(year+" Leap Year");
	}
}
