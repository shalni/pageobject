
////0410 Daily Question - To print all country names in IRCTC Sign Up Page

package DailyPrograms;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Countries {
public static void main(String[] args) {
	
		System.setProperty("webdriver.chrome.driver", "C:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		Select countries=new Select(driver.findElementById("userRegistrationForm:countries"));
		List<WebElement> allCountries=countries.getOptions();
		for(WebElement eachCountry:allCountries)
			System.out.println(eachCountry.getText());
}
}
