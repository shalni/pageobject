//Home Work 30-Sep

package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {
	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "C:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementByXPath("//input[@id='username']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		driver.findElementByXPath("//a[contains(@href,'crmsfa')]").click();
		driver.findElementByXPath("//a[contains(@href,'leadsMain')]").click();
		driver.findElementByXPath("//a[contains(@href,'createLeadForm')]").click();
		driver.findElementByXPath("//input[@id='createLeadForm_companyName']").sendKeys("RALFSoft");
		driver.findElementByXPath("//input[@id='createLeadForm_firstName']").sendKeys("Shruthi");
		driver.findElementByXPath("//input[@id='createLeadForm_lastName']").sendKeys("Rakesh");
		Select source=new Select(driver.findElementByXPath("//select[@id='createLeadForm_dataSourceId']"));
		source.selectByValue("LEAD_EMPLOYEE");
		Select industry=new Select(driver.findElementByXPath("//select[@id='createLeadForm_industryEnumId']"));
		industry.selectByVisibleText("Manufacturing");
		driver.findElementByXPath("//input[@name='submitButton']").click();
		driver.findElementByXPath("//a[contains(@href,'findLeads')]").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Shruthi");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		List<WebElement> allRows=driver.findElementsByXPath("(//table[@class='x-grid3-row-table'])[1]/tbody/tr");
		int rowsCount=allRows.size();
		System.out.println("rowsCount "+rowsCount);
		WebElement eachLeadRow=allRows.get(0);
		List<WebElement> allLeadColumns=eachLeadRow.findElements(By.tagName("a"));
		allLeadColumns.get(0).click();
	}
}
