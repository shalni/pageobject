package week3.day2;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class PrintTrains {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://erail.in");
		driver.manage().window().maximize();
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);
		Thread.sleep(3000);
		WebElement ele= driver.findElementById("chkSelectDateOnly");
		if(ele.isSelected()==true)
		{ele.click();
		}
		
		Thread.sleep(3000);
		List<WebElement> allRows=driver.findElementsByXPath("//table[@class='DataTable TrainList']/tbody/tr");
		System.out.println(allRows.size());
		for(int i=0;i<allRows.size();i++)
		{WebElement eachTrainRow=allRows.get(i);
		 List<WebElement> allTrainColumns=eachTrainRow.findElements(By.tagName("td"));
		 System.out.println(allTrainColumns.get(1).getText());
		}
		
		WebElement rowThree= allRows.get(2);
		System.out.println("Details of Train# 3");
		List<WebElement> allColumns=rowThree.findElements(By.tagName("td"));
		for(WebElement eachColValue:allColumns)
		{
			System.out.print(" "+eachColValue.getText());
		}
			
	}

}
