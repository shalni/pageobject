package week3.day1;


import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByPartialLinkText("CRM/SFA").click();
		driver.findElementByPartialLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Ralfsoft Technologies");
		driver.findElementById("createLeadForm_firstName").sendKeys("Shalni");
		driver.findElementById("createLeadForm_lastName").sendKeys("Venkateswaran");
		Select eleDataSource= new Select(driver.findElementById("createLeadForm_dataSourceId"));
		eleDataSource.selectByVisibleText("Public Relations");
		Select eleMarkCamp= new Select(driver.findElementById("createLeadForm_marketingCampaignId"));
		eleMarkCamp.selectByValue("CATRQ_AUTOMOBILE");
		Select eleIndus= new Select(driver.findElementById("createLeadForm_industryEnumId"));
		List<WebElement> allIndus=eleIndus.getOptions();
		System.out.println("Printing the Industries strating with letter M:");
		for(WebElement eachIndus:allIndus)
		{if(eachIndus.getText().startsWith("M"))
			System.out.println(eachIndus.getText());
		}
		driver.findElementByName("submitButton").click();
		driver.findElementByPartialLinkText("Delete").click();
		driver.close();
		
		/*driver.findElementByPartialLinkText("Leads");
		driver.findElementByPartialLinkText("Find Leads");
		driver.findElementByName("firstName").sendKeys("Shalni");
		driver.findElementByPartialLinkText("Find Leads");*/
		
	}

}
