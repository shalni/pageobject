package week3.day1;

//import java.awt.RenderingHints.Key;
import java.util.List;

//import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CountLinks {
	public static void main(String[] args) throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		List<WebElement> countTags=driver.findElementsByTagName("input");
		int size=countTags.size();
		System.out.println("Number of Anchor Tags:"+size);
		driver.manage().window().maximize();
		driver.findElementById("userRegistrationForm:userName").sendKeys("shalni41");
		driver.findElementById("userRegistrationForm:checkavail").click();
		Thread.sleep(3000);
		WebElement text=driver.findElementByXPath("//label[@id='userRegistrationForm:useravail']");
		System.out.println(text.getText());
		if((text.getText().contains("User Id is Available"))==true)
		{driver.findElementById("userRegistrationForm:password").sendKeys("railway");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("railway");
		Select question=new Select(driver.findElementById("userRegistrationForm:securityQ"));
		question.selectByIndex(3);
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Vijay");
		Select lang=new Select(driver.findElementById("userRegistrationForm:prelan"));
		lang.selectByVisibleText("English");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Shalni");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Venkateswaran");
	    driver.findElementById("userRegistrationForm:gender:1").click();
	    driver.findElementById("userRegistrationForm:maritalStatus:0").click();
	    Select dateBirth=new Select(driver.findElementById("userRegistrationForm:dobDay"));
	    dateBirth.selectByVisibleText("13");
	    Select monthBirth=new Select(driver.findElementById("userRegistrationForm:dobMonth"));
	    monthBirth.selectByVisibleText("MAY");
	    Select yearBirth=new Select(driver.findElementById("userRegistrationForm:dateOfBirth"));
	    yearBirth.selectByValue("1982");
	    Select occupation=new Select(driver.findElementById("userRegistrationForm:occupation"));
	    occupation.selectByVisibleText("Private");
	    Select countries=new Select(driver.findElementById("userRegistrationForm:countries"));
	    countries.selectByValue("94");
	    driver.findElementById("userRegistrationForm:email").sendKeys("shalni41@gmail.com");
	    driver.findElementById("userRegistrationForm:mobile").sendKeys("9790904770");
	    Select nationality=new Select(driver.findElementById("userRegistrationForm:nationalityId"));
	    nationality.selectByValue("94");
	    driver.findElementById("userRegistrationForm:address").sendKeys("Plot No.5 Nakeeran Street");
	    driver.findElementById("userRegistrationForm:pincode").sendKeys("600073",Keys.TAB);
	    Thread.sleep(3000);
	    Select city=new Select(driver.findElementById("userRegistrationForm:cityName"));
	    city.selectByIndex(1);
	    Thread.sleep(3000);
	    Select postoffice=new Select(driver.findElementById("userRegistrationForm:postofficeName"));
	    postoffice.selectByIndex(1);
	    driver.findElementById("userRegistrationForm:landline").sendKeys("04422271189");
	    driver.findElementById("userRegistrationForm:resAndOff").click();
		}
		else
			System.out.println("Duplicate User Id");
	}
}
