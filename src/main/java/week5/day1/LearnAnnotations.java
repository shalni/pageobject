package week5.day1;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class LearnAnnotations {
	int i;
	@BeforeSuite
	public void beforeSuite()
	{System.out.println("#"+i+ "Before Suite");
	 i++;
	}

	@BeforeTest
	public void beforeTest()
	{System.out.println("#"+i+"Before Test");
	 i++;
	}
	
	@BeforeClass
	public void beforeClass()
	{System.out.println("#"+i+"Before Class");
	i++;
	}
	
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("#"+i+"Before Method");
		i++;
	}
	
	@Test
	public void test1()
	{
	 System.out.println("Test "+i);
	 i++;
	}
	
	@Test
	public void test2()
	{
	 System.out.println("Test "+i);
	 i++;
	}
	
	@AfterMethod
	public void afterMethod()
	{System.out.println("#"+i+"After Method");
	i++;
	}
	
	@AfterSuite
	public void afterSuite()
	{System.out.println("#"+i+"After Suite");
	 i++;
	}

	@AfterTest
	public void afterTest()
	{System.out.println("#"+i+"After Test");
	i++;
	}
	
	@AfterClass
	public void afterClass()
	{System.out.println("#"+i+"After Class");
	i++;
	}
	

}
