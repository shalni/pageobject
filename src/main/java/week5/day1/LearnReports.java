package week5.day1;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.*;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class LearnReports {
	@Test
public void generateReport() throws IOException
{ //Create a HTML File for presenting Report, Ensure the report folder is existing in the mentioned path
		ExtentHtmlReporter htmlFile=new ExtentHtmlReporter("./reports/result.html");
  //Allows to maintain History of the result files if true or replaces the existing version false
		htmlFile.setAppendExisting(true);
	//Create a object for editing a file
		ExtentReports editFile=new ExtentReports();
    //Write all the information captured in editFile to HTML File
	editFile.attachReporter(htmlFile);
	ExtentTest logger=editFile.createTest("TC001_CreateLead", "Creates a Lead Record");
	//Reports the author executing the testcase
	logger.assignAuthor("Shalni");
	//Reports the type of Test like Regression
	logger.assignCategory("New Test");
	//Logging all the steps of the testcase mentioning the status, description & screenshot. And ensure the screenshot folder exits in the report folder
	logger.log(Status.PASS, "UserName Entered Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
	logger.log(Status.PASS, "Password Entered Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img2.png").build());
	logger.log(Status.PASS, "Login Button Clicked Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img3.png").build());
	//Flushes all the information captured to Memory
	editFile.flush();
}
}
