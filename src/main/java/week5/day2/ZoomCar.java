package week5.day2;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import wdMethods.SeMethods;

public class ZoomCar extends SeMethods{
	
	@BeforeTest
	public void testcaseValues()
	{String testcaseName="TC001_ZoomCar";
	 String testcaseDescription="To Create a Journey";
			 String author="Shalni";
	 String category="AC Execution";
	 runTestCase(testcaseName,testcaseDescription,author,category);
	}
	
	@Test
	public void zoomCar() {
		String brand;
		startApp("chrome", "https://www.zoomcar.com/chennai/");
		WebElement searchCar =  locateElement("linktext","Start your wonderful journey");
		click(searchCar);
		//switchToWindow(1);
		WebElement pickPlace=locateElement("xpath","(//div[@class='items'])[3]");
		click(pickPlace);
		WebElement nextButton=locateElement("xpath","//button[@class='proceed']");
		click(nextButton);
		WebElement nextDate=locateElement("xpath","(//div[@class='day'])[1]");
		nextDate.click();
		nextButton=locateElement("xpath","//button[@class='proceed']");
		click(nextButton);
		
		WebElement selectedDatePicked=locateElement("xpath","//div[@class='day picked ']");
		//System.out.println(selectedDatePicked.getText());
		String datePicked=selectedDatePicked.getText().replaceAll("\\D", "");
		//int startDatePicked=Integer.parseInt(datePicked);
		Date date=new Date();
		DateFormat sdf=new SimpleDateFormat("dd");
		String today=sdf.format(date);
		int tomorrow=Integer.parseInt(today)+1;
		String tom=String.valueOf(tomorrow);
		System.out.println(datePicked+"dates"+tom);
		if(datePicked.equals(tom))
		teststepResult(Status.PASS,"Start Dates are Verified");
		WebElement doneButton=locateElement("xpath","//button[@class='proceed']");
		click(doneButton);
	    //WebElement price=locateElement("xpath","(//div[@class='price'])[1]");
		//List<WebElement> prices=driver.findElementsByXPath("//div[@class='price']");
		List<WebElement> prices=locateElements("xpath","//div[@class='price']");
		ArrayList<String> pricesList=new ArrayList<>();
		for(WebElement eachPrice:prices)
		pricesList.add((eachPrice.getText().replaceAll("\\D", "")));
		String verifyPrice=Collections.max(pricesList);
	    i=1;
	    for(WebElement eachPrice:prices)
	    {if(eachPrice.getText().contains(verifyPrice))
		 {String brandXpath = "(//h3)["+i+"]";
		  WebElement  brandText=locateElement("xpath",brandXpath);
		  brand=getText(brandText);
		  String bookNowXpath="(//button[@name='book-now'])["+i+"]";
		  WebElement bookNowButton=locateElement("xpath",bookNowXpath);
		  System.out.println("Brand Xpath"+bookNowXpath);
		  click(bookNowButton);
		  teststepResult(Status.PASS,"Highest Brand Name and Price"+brand+" "+verifyPrice);
		  break;
		 }
	     i++;
	     }
	     closeBrowser();
	    }	
	}

