package week5.day2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import wdMethods.SeMethods;

public class Facebook extends SeMethods{
	
	@BeforeTest
	public void testcaseValues()
	{String testcaseName="TC002_FaceBook";
	 String testcaseDescription="To Count Reviews";
			 String author="Shalni";
	 String category="AC Execution";
	 runTestCase(testcaseName,testcaseDescription,author,category);
	}
	
	@Test
	
	public void accessFacebook() throws InterruptedException
	{
		startApp("chrome", "https://www.facebook.com");
		WebElement userName=locateElement("email");
		clear(userName);
		type(userName,"shalni41@gmail.com");
		WebElement password=locateElement("pass");
		clear(password);
		type(password,"facebookshalni");
		WebElement signInButton=locateElement("xpath","//input[@value='Log In']");
		click(signInButton);
		WebElement searchBox=locateElement("name","q");
		//clear(searchBox);
		type(searchBox,"TestLeaf");
		Thread.sleep(3000);
		WebElement searchButton=locateElement("xpath","//button[@aria-label='Search']");
		click(searchButton);
		WebElement likeButton=locateElement("xpath","(//button[contains(text(),'Like')])[1]");
		System.out.println(likeButton.getText());
	    /*if((likeButton.getText()).equals("Like"))
		click(likeButton);*/
		System.out.println("Like Status" +likeButton.getText());
		click(likeButton);
		teststepResult(Status.PASS, "Likes the Page");
		WebElement testLeafPage=locateElement("xpath","(//div[contains(text(),'TestLeaf')])[1]");
		click(testLeafPage);
		WebElement clickReview=locateElement("xpath","//span[text()='Reviews']");
		click(clickReview);
		List<WebElement> allReviews=locateElements("xpath","//span[contains(text(),'reviewed')]");
		//List<WebElement> allReviews=driver.findElementsByXPath("//span[contains(text(),'reviewed')]");		
		teststepResult(Status.PASS, "Reviews "+allReviews.size());
		WebElement clickCommunity=locateElement("xpath","//span[text()='Community']");
		click(clickCommunity);
		WebElement getlikeCounts=locateElement("xpath","(//div[@class='_3xom'])[1]");
		System.out.println("counts"+getText(getlikeCounts));
		
		}

}
