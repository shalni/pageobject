package week2.day1;

public class SmartTV extends Television
{
 public void listenMusic()
 {
	 System.out.println("Listening Music");
 }
 
 public void connectWiFi()
 {
	 System.out.println("Connected to WiFi");
 }
 
 public void installApp()
 {
	 System.out.println("Youtube Installed");
	 
 }
 
 @Override
 
 public void changeChannel()
	{
		System.out.println("Change Channel to Vijay TV");
	}
 @Override
	public void getSubscription() {
		// TODO Auto-generated method stub
		System.out.println("Subscribed to TATASky Network");
		
	}

	@Override
	public void selectPackage() {
		// TODO Auto-generated method stub
		System.out.println("Selected North Indian Package");
	}
	
 public static void main(String[] args) 
 {Television tv= new Television();
  SmartTV stv=new SmartTV();
  Entertainment etv=new SmartTV();
  Entertainment estv=new SmartTV();
  
  System.out.println("TV Options: ");
  tv.getSubscription();
  tv.selectPackage();
  tv.switchOn();
  tv.changeChannel();
  tv.adjustVolume();
  System.out.println("");
  System.out.println("SmartTV Options: ");
  stv.getSubscription();
  stv.selectPackage();
  stv.switchOn();
  stv.changeChannel();
  stv.adjustVolume();
  stv.listenMusic();
  stv.connectWiFi();
  stv.installApp();
  System.out.println("");
  System.out.println("Entertainment TV Options: ");
  etv.getSubscription();
  etv.selectPackage();
  System.out.println("");
  System.out.println("Entertainment SmartTV Options: ");
  estv.getSubscription();
  estv.selectPackage();

}
}
