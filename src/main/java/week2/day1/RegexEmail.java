package week2.day1;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexEmail
	{
		public static void main(String[] args)
		{String email;
		 Scanner scan=new Scanner(System.in);
		 System.out.println("Enter the Email Id:");
		 email=scan.next();
		 Pattern pat=Pattern.compile("[a-zA-Z]{5,}[a-zA-Z0-9]{1,}@[a-z]{3,}\\.[a-z]{2,}");
		 Matcher matcher=pat.matcher(email);
		 scan.close();
		if(matcher.matches()==true)
			System.out.println(email+" is valid Email Id");
		else
			System.out.println(email+" is invalid Email Id");
		}
}
