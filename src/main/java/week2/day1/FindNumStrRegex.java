package week2.day1;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindNumStrRegex {
	public static void main(String[] args) 
	{
		 String str;
		 Scanner scan=new Scanner(System.in);
		 System.out.println("Enter the String:");
		 str=scan.next();
		 Pattern pat=Pattern.compile("[0-9]");
		 scan.close();
		 System.out.println("Numbers in the String:");
		 for(int i=0;i<str.length();i++)
		 {Matcher matcher=pat.matcher(str.substring(i,i+1));
		  if(matcher.matches()==true)
		   System.out.print(" "+str.substring(i,i+1));
		 }
	}

}
