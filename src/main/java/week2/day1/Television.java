package week2.day1;

public class Television implements Entertainment
{
	public void switchOn()
	{
		System.out.println("Switch on TV");
	}
	
	public void changeChannel()
	{
		System.out.println("Change Channel to SUN TV");
	}
	
	public void adjustVolume()
	{
		System.out.println("Adjust Volumes");
	}

	@Override
	public void getSubscription() {
		// TODO Auto-generated method stub
		System.out.println("Subscribed to SUN Network");
		
	}

	@Override
	public void selectPackage() {
		// TODO Auto-generated method stub
		System.out.println("Selected South Indian Package");
	}
	
}
