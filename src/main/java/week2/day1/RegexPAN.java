package week2.day1;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexPAN 
{
	public static void main(String[] args)
	{String pan;
	 Scanner scan=new Scanner(System.in);
	 System.out.println("Enter the PAN Number:");
	 pan=scan.next();
	 Pattern pat=Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");
	 Matcher matcher=pat.matcher(pan);
	 scan.close();
	 System.out.println(matcher.matches());
	}
}
