package week2.day1;

public class PocketRadio extends Radio {
	
	public  void changeBattery()
	{
	 System.out.println("Battery changed");
	}
	
	public static void main(String[] args) 
	{
		//Radio r=new Radio();
		PocketRadio pr= new PocketRadio();
		pr.playStation();
		pr.changeBattery();
	}

}
