package utils;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {
	ExtentHtmlReporter htmlFile;
	ExtentReports editFile;
	ExtentTest logger;
	@BeforeSuite
	public void startResult()
	{//Create a HTML File for presenting Report, Ensure the report folder is existing in the mentioned path
		htmlFile=new ExtentHtmlReporter("./reports/result.html");
     //Allows to maintain History of the result files if true or replaces the existing version false
		htmlFile.setAppendExisting(true);
	   //Create a object for editing a file
		editFile=new ExtentReports();
		editFile.attachReporter(htmlFile);
	}
	
	
	
	public void runTestCase(String testcaseName, String testcaseDescription, String author, String category)
	{//Write all the information captured in editFile to HTML File
	 logger=editFile.createTest(testcaseName,testcaseDescription);
	//Reports the author executing the testcase
	  logger.assignAuthor(author);
	//Reports the type of Test like Regression
	  logger.assignCategory(category);
	}
	//Logging all the steps of the testcase mentioning the status, description & screenshot. And ensure the screenshot folder exits in the report folder
	
	public void teststepResult(Status s, String message)
	{logger.log(s,message);
	}
	
	@AfterSuite
	public void endResult()
	 {editFile.flush();
	 }
	

}
