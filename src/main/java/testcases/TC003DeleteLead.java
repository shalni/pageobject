package testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;


public class TC003DeleteLead extends ProjectMethods
{
	@BeforeTest
	public void testcaseValues()
	{String testcaseName="TC003_Delete_Lead";
	 String testcaseDescription="To Delete Lead";
			 String author="Shalni";
	 String category="AC Execution";
	 runTestCase(testcaseName,testcaseDescription,author,category);
	}
	
   @Test
	public void deleteLead() throws InterruptedException
	{WebElement findLead = locateElement("xpath","//a[contains(@href,'findLeads')]");
	 click(findLead);
	 WebElement phoneTab=locateElement("xpath","//span[text()='Phone']");
	 click(phoneTab);
	 WebElement findPhoneNumber=locateElement("name","phoneNumber");
	 type(findPhoneNumber,"9790904770");
	 WebElement findLeadButton=locateElement("xpath","//button[text()='Find Leads']");
	 click(findLeadButton);
	 Thread.sleep(2000);
	 List<WebElement> allRows=driver.findElementsByXPath("(//table[@class='x-grid3-row-table'])[1]/tbody/tr");
	 int rowsCount=allRows.size();
	 System.out.println("rowsCount "+rowsCount);
	 WebElement eachLeadRow=allRows.get(0);
	 List<WebElement> allLeadColumns=eachLeadRow.findElements(By.tagName("a"));
	 String leadIdDeleted=getText(allLeadColumns.get(0));
	 click(allLeadColumns.get(0));
	 WebElement deleteLeadButton = locateElement("linktext","Delete");
	 click(deleteLeadButton);
	 WebElement findLead2 = locateElement("xpath","//a[contains(@href,'findLeads')]");
	 click(findLead2);
	 WebElement eleLeadId=locateElement("name","id");
	 type(eleLeadId,leadIdDeleted);
	 WebElement findLeadButton2=locateElement("xpath","//button[text()='Find Leads']");
	 click(findLeadButton2);
	 WebElement eleMessage=locateElement("class","x-paging-info");
	 verifyExactText(eleMessage,"No records to display");
	}
}
