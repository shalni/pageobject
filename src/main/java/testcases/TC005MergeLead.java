package testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC005MergeLead extends ProjectMethods{
	
	@BeforeTest
	public void testcaseValues()
	{String testcaseName="TC005 Merge_Lead";
	 String testcaseDescription="To Merge Lead";
			 String author="Shalni";
	 String category="AC Execution";
	 runTestCase(testcaseName,testcaseDescription,author,category);
	}
	
	@Test
	public void mergeLead() throws InterruptedException
	{WebElement mergeLead=locateElement("xpath","//a[contains(@href,'mergeLeads')]");
	 click(mergeLead);
	 WebElement firstLookupImage=locateElement("xpath","(//img[@alt='Lookup'])[1]");
	 click(firstLookupImage);
	 switchToWindow(1);
	 
	 WebElement firstName1=locateElement("name","firstName");
	 type(firstName1,"Shalni");
	 WebElement findLeadButton=locateElement("xpath","//button[text()='Find Leads']");
	 click(findLeadButton);
	 Thread.sleep(2000);
	 List<WebElement> allRows=driver.findElementsByXPath("(//table[@class='x-grid3-row-table'])[1]/tbody/tr");
	 WebElement eachLeadRow=allRows.get(0);
	 List<WebElement> allLeadColumns=eachLeadRow.findElements(By.tagName("a"));
	 String fromLeadId=getText(allLeadColumns.get(0));
	 System.out.println(fromLeadId);
	 click(allLeadColumns.get(0));
	 
	 switchToWindow(0);
	 WebElement fromLeadNumber=locateElement("ComboBox_partyIdFrom");
	 Thread.sleep(1000);
	 System.out.println(fromLeadNumber.getText());
	 verifyExactAttribute(fromLeadNumber,"value",fromLeadId);
	 WebElement secondLookupImage=locateElement("xpath","(//img[@alt='Lookup'])[2]");
	 click(secondLookupImage);
	 switchToWindow(1);
	 
	 WebElement firstName2=locateElement("name","firstName");
	 type(firstName2,"Shruthi");
	 WebElement findLeadButton2=locateElement("xpath","//button[text()='Find Leads']");
	 click(findLeadButton2);
	 Thread.sleep(2000);
	 List<WebElement> allRows2=driver.findElementsByXPath("(//table[@class='x-grid3-row-table'])[1]/tbody/tr");
	 WebElement eachLeadRow2=allRows2.get(0);
	 List<WebElement> allLeadColumns2=eachLeadRow2.findElements(By.tagName("a"));
	 String toLeadId=getText(allLeadColumns2.get(0));
	 click(allLeadColumns2.get(0));
	 switchToWindow(0);
	 
	 WebElement toLeadNumber=locateElement("ComboBox_partyIdTo");
	 Thread.sleep(1000);
	 verifyExactAttribute(toLeadNumber,"value", toLeadId);
	 WebElement mergeButton=locateElement("linktext","Merge");
	 click(mergeButton);
	 acceptAlert();
	 
	 WebElement findLead = locateElement("xpath","//a[contains(@href,'findLeads')]");
	 click(findLead);
	 WebElement eleLeadId=locateElement("name","id");
	 type(eleLeadId,fromLeadId);
	 WebElement findLeadButton3=locateElement("xpath","//button[text()='Find Leads']");
	 click(findLeadButton3);
	 WebElement eleMessage=locateElement("class","x-paging-info");
	 verifyExactText(eleMessage,"No records to display");
	}
}
