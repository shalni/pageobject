package testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC002EditLead extends ProjectMethods
{
	@BeforeTest
	public void testcaseValues()
	{String testcaseName="TC002_Edit_Lead";
	 String testcaseDescription="To Edit Lead";
			 String author="Shalni";
	 String category="AC Execution";
	 runTestCase(testcaseName,testcaseDescription,author,category);
	}
	
    @Test
	public void editLead() throws InterruptedException
	{WebElement findLead = locateElement("xpath","//a[contains(@href,'findLeads')]");
	 click(findLead);
	 WebElement firstName=locateElement("xpath","(//input[@name='firstName'])[3]");
	 type(firstName,"Shruthi");
	 WebElement findLeadButton=locateElement("xpath","//button[text()='Find Leads']");
	 click(findLeadButton);
	 Thread.sleep(2000);
	 List<WebElement> allRows=driver.findElementsByXPath("(//table[@class='x-grid3-row-table'])[1]/tbody/tr");
	 int rowsCount=allRows.size();
	 System.out.println("rowsCount "+rowsCount);
	 WebElement eachLeadRow=allRows.get(0);
	 List<WebElement> allLeadColumns=eachLeadRow.findElements(By.tagName("a"));
	 click(allLeadColumns.get(0));
	 Thread.sleep(3000);
	 boolean b=verifyTitle("View Lead | opentaps CRM");
	 if(b==true)
		 System.out.println("Title displayed as expected");
	 else
		 System.out.println("Title displayed is not as expected");
	 WebElement editButton=locateElement("linktext","Edit");
	 click(editButton);
	 WebElement companyName=locateElement("xpath","(//input[@name='companyName'])[2]");
	 String expectedCompanyName="Visual Soft";
	companyName.clear();
	 type(companyName,expectedCompanyName);
	 WebElement updateButton=locateElement("xpath","(//input[@name='submitButton'])[1]");
	 click(updateButton);
	 Thread.sleep(3000);
	 WebElement viewCompanyName=locateElement("xpath","//span[@id='viewLead_companyName_sp']");
	 verifyPartialText(viewCompanyName,expectedCompanyName);
	}

}
