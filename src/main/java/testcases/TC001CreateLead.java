package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import wdMethods.ProjectMethods;
import week6.day2.ReadExcelSheet;


public class TC001CreateLead extends ProjectMethods 
{
	@BeforeTest
	public void testcaseValues()
	{String testcaseName="TC001_Create_Lead";
	 String testcaseDescription="To Create TestLead";
			 String author="Shalni";
	 String category="AC Execution";
	 runTestCase(testcaseName,testcaseDescription,author,category);
	}
	
	@Test(dataProvider="createlead")
	public void createLead(String cname,String fname, String lname,String email, String phone)
	{
	 WebElement createLeadPage = locateElement("xpath","//a[contains(@href,'createLeadForm')]");
	 createLeadPage.click();
	 WebElement companyName=locateElement("createLeadForm_companyName");
	 type(companyName,cname);
	 WebElement firstName=locateElement("createLeadForm_firstName");
	 type(firstName,fname); 
	 WebElement lastName=locateElement("createLeadForm_lastName");
	 type(lastName,lname);
	 WebElement source=locateElement("createLeadForm_dataSourceId");
	 selectDropDownUsingText(source,"Public Relations");
	 WebElement marketingCampaign= locateElement("createLeadForm_marketingCampaignId");
	 selectDropDownUsingText(marketingCampaign,"Automobile");
	 WebElement industry=locateElement("createLeadForm_industryEnumId");
	 selectDropDownUsingIndex(industry,3);
	 WebElement createLeadButton=locateElement("name","submitButton");
	 WebElement elePhone=locateElement("createLeadForm_primaryPhoneNumber");
	 type(elePhone,phone);
	 WebElement eleEmail=locateElement("createLeadForm_primaryEmail");
	 clear(eleEmail);
	 type(eleEmail,email);
	 click(createLeadButton);
	}
	
	@DataProvider(name="createlead",indices= {2}) //indices allows to select the rows for which the data needs to be inserted
	public Object[][] fetchData() throws IOException
	{Object[][] data=ReadExcelSheet.readExcel();
	 /*data[0][0]="WIPRO";
	 data[0][1]="Shalni";
	 data[0][2]="Gurrala";
	 data[0][3]="shalni@gmail.com";
	 data[0][4]="1234567890";
	 
	 data[1][0]="RalfSoft";
	 data[1][1]="Shruthi";
	 data[1][2]="Rakesh";
	 data[1][3]="shruthi@gmail.com";
	 data[1][4]="7777777777";*/
     return data;
	}

}
