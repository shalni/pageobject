package testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;



public class TC004DuplicateLead extends ProjectMethods {
	
	@BeforeTest
	public void testcaseValues()
	{String testcaseName="TC004_Duplicate_Lead";
	 String testcaseDescription="To Duplicate Lead";
			 String author="Shalni";
	 String category="AC Execution";
	 runTestCase(testcaseName,testcaseDescription,author,category);
	}
	
	@Test
	public void duplicateLead() throws InterruptedException
	{WebElement findLead = locateElement("xpath","//a[contains(@href,'findLeads')]");
	 click(findLead);
	 WebElement emailTab=locateElement("xpath","//span[text()='Email']");
	 click(emailTab);
	 WebElement eleEmail=locateElement("name","emailAddress");
	 type(eleEmail, "shalni@gmail.com");
	 WebElement findLeadButton=locateElement("xpath","//button[text()='Find Leads']");
	 click(findLeadButton);
	 Thread.sleep(2000);
	 List<WebElement> allRows=driver.findElementsByXPath("(//table[@class='x-grid3-row-table'])[1]/tbody/tr");
	 int rowsCount=allRows.size();
	 System.out.println("rowsCount "+rowsCount);
	 WebElement eachLeadRow=allRows.get(0);
	 List<WebElement> allLeadColumns=eachLeadRow.findElements(By.tagName("a"));
	 String expectedLeadName=getText(allLeadColumns.get(2));
	 click(allLeadColumns.get(2));
	 WebElement duplicateButton=locateElement("linktext","Duplicate Lead");
	 click(duplicateButton);
	 boolean b=verifyTitle("Duplicate Lead | opentaps CRM");
	 if(b==true)
		 System.out.println("Title displayed as expected");
	 else
		 System.out.println("Title displayed is not as expected");
	 WebElement createLeadButton=locateElement("class","smallSubmit");
	 click(createLeadButton);
	 WebElement savedFirstName=locateElement("viewLead_firstName_sp");
	 verifyExactText(savedFirstName,expectedLeadName);
	}
}
