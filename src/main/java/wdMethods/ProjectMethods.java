package wdMethods;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterMethod;

import wdMethods.SeMethods;

public class ProjectMethods extends SeMethods{
	
	@Parameters({"url","username","password"})
	@BeforeMethod
	public void login(String url, String username, String password) {
		startApp("chrome",url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword,password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement crmsfaClick = locateElement("xpath","//a[contains(@href,'crmsfa')]");
		click(crmsfaClick);
		WebElement leadPage = locateElement("xpath","//a[contains(@href,'leadsMain')]");
		leadPage.click();
	}
	
	@AfterMethod
	public void closeApp()
	{closeBrowser();	
	}
	
	
	
}












