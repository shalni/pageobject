package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.swing.plaf.synth.SynthScrollBarUI;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import utils.Reports;

public class SeMethods extends Reports implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeOptions option = new ChromeOptions();
				option.addArguments("--disable-notifications");
				driver = new ChromeDriver(option);
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			teststepResult(Status.PASS,"The Browser "+browser+" Launched Successfully");
			//System.out.println("The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			//System.out.println("The Browser "+browser+" not Launched ");
			teststepResult(Status.FAIL,"The Browser "+browser+"not launched");
		} finally {
			takeSnap();			
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "name": return driver.findElementByName(locValue);
			case "linktext": return driver.findElementByLinkText(locValue);
			}
			System.out.println("The Element "+locator+"located Successfully");
		} catch (NoSuchElementException e) {
			teststepResult(Status.FAIL,"The Element "+locator+" not located Successfully");
		}
		
		return null;
	}

	public List<WebElement> locateElements(String locator, String locValue) {
		try {
			 List<WebElement> listItems=driver.findElementsByXPath(locValue);
			 return listItems;
			}
		
		 catch (NoSuchElementException e) {
			teststepResult(Status.FAIL,"The Element "+locator+" not located Successfully");
		}
		
		return null;
	}
	@Override
	public WebElement locateElement(String locValue) {
		try {
		return driver.findElementById(locValue);
		}
		
		catch (NoSuchElementException e) {
			teststepResult(Status.FAIL,"The Element "+locValue+" not located Successfully");
		}
		return null;

	}

	@Override
	public void type(WebElement ele, String data) {
		try {
		ele.sendKeys(data);
		//System.out.println("The Data "+data+" is Entered Successfully");
		teststepResult(Status.PASS,"The Data "+data+" is entered Successfully");
		}
		catch (NoSuchElementException e) {
			teststepResult(Status.FAIL,"The Data "+data+" is not entered Successfully");
		}
		finally
		{
			takeSnap();
		}
	}

	@Override
	public void click(WebElement ele) {
		try {
		ele.click();
		teststepResult(Status.PASS,"The Element "+ele+"clicked Successfully");
		}
		catch (NoSuchElementException e) {
			teststepResult(Status.FAIL,"The Element "+ele+" not clicked Successfully");
		}
	}
	
	@Override
	public void clear(WebElement ele)
	{
		try {
		ele.clear();
		teststepResult(Status.PASS, "The text in Element "+ele+" cleared Successfully");
		}
		catch (NoSuchElementException e) {
			teststepResult(Status.FAIL, "The text in Element "+ele+" not cleared Successfully");
		}
		finally {
		takeSnap();
		}
	}
	
	@Override
	public String getText(WebElement ele) {
		try {
		String text = ele.getText();
		return text;
		}
		catch (NoSuchElementException e) {
			teststepResult(Status.FAIL, "Unable to get text from element"+ele);
		}
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
		Select dd = new Select(ele);
		dd.selectByVisibleText(value);
		//System.out.println("The DropDown Is Selected with "+value);
		teststepResult(Status.PASS, "The DropDown is selected with "+value);
		}
		catch (NoSuchElementException e) {
			teststepResult(Status.FAIL, "The DropDown is not selected with "+value);
		}
		finally 
		{
			takeSnap();			
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
		Select dd = new Select(ele);
		dd.selectByIndex(index);
		//System.out.println("The DropDown Is Selected with "+index);
		teststepResult(Status.PASS, "The DropDown is selected with "+index);
		}
		catch (NoSuchElementException e) {
			teststepResult(Status.FAIL, "The DropDown is not selected with "+index);
		}
		finally 
		{
			takeSnap();			
		}

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		boolean v=true;
		try{
			if(driver.getTitle().equals(expectedTitle))
				v= true;
		else
		v= false;
			//System.out.println(driver.getTitle());
			teststepResult(Status.PASS, "Found the title");
		}
		catch (NoSuchElementException e) {
			teststepResult(Status.FAIL, "No element Found");
		}
		finally 
		{
			takeSnap();			
		}
	 return v;
		
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
		if(ele.getText().equals(expectedText))
		   teststepResult(Status.PASS, ele.getText()+" Matches "+expectedText);
		else
		   teststepResult(Status.FAIL,ele.getText()+" Do not match "+expectedText);
		}
		catch (NoSuchElementException e) {
			teststepResult(Status.FAIL, "No element Found");
		}
		finally 
		{
			takeSnap();			
		}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
		if(ele.getText().contains(expectedText))
			teststepResult(Status.PASS, ele.getText()+" contains "+expectedText);
		else
			teststepResult(Status.FAIL,ele.getText()+" does not contain "+expectedText);
		}
		catch (NoSuchElementException e) {
			teststepResult(Status.FAIL, "No element Found");
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		try {
		if(ele.getAttribute(attribute).equals(value))
			teststepResult(Status.PASS,ele.getAttribute(attribute)+ "Attribute Value Matches with "+value);
		else
			teststepResult(Status.FAIL,ele.getAttribute(attribute)+ " Attribute value do not match with "+value);
		}
		catch (NoSuchElementException e) {
			teststepResult(Status.FAIL, "No element Found");
		}
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
		if(ele.getAttribute(attribute).contains(value))
			teststepResult(Status.PASS,ele.getAttribute(attribute)+ "Attribute contains value "+value);
		else
			teststepResult(Status.FAIL,ele.getAttribute(attribute)+ " Attribute do not contain value "+value);
		}
		catch (NoSuchElementException e) {
			teststepResult(Status.FAIL, "No element Found");
		}
		
	}

	@Override
	public void verifySelected(WebElement ele) {
		try {
		if(ele.isSelected())
		teststepResult(Status.PASS,ele+" WebElement Selected");
		else 
		teststepResult(Status.FAIL,ele+" WebElement not selected");
		}
		catch (NoSuchElementException e) {
			teststepResult(Status.FAIL, "No element Found");
		}
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		try {
		if(ele.isDisplayed())
			teststepResult(Status.PASS,ele+" WebElement Displayed");
		else 
			teststepResult(Status.PASS,ele+" WebElement not Displaye");
		}
		catch (NoSuchElementException e) {
			teststepResult(Status.FAIL, "No element Found");
		}

	}

	@Override
	public void switchToWindow(int index) {
		try {
		Set<String> allWindows = driver.getWindowHandles();
		List<String> listOfWindow = new ArrayList<String>();
		listOfWindow.addAll(allWindows);
		driver.switchTo().window(listOfWindow.get(index));
		teststepResult(Status.PASS,"The Window is Switched ");
		}
		catch (NoSuchWindowException e) {
			teststepResult(Status.FAIL, "No Window Found");
		}
		finally 
		{
			takeSnap();			
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
		driver.switchTo().frame(ele);
		teststepResult(Status.PASS,"The Window is Switched ");
		}
		catch (NoSuchFrameException e) {
			teststepResult(Status.FAIL, "No Frame Found");
		}

	}

	@Override
	public void acceptAlert() {
		try {
		driver.switchTo().alert().accept();
		teststepResult(Status.PASS,"Accepted Alert Successfully");
		}
		catch (Exception e) {
			teststepResult(Status.FAIL, "No Alert Found");
		}
	}

	@Override
	public void dismissAlert() {
		try {
		driver.switchTo().alert().dismiss();
		teststepResult(Status.PASS,"Dismissed Alert Successfully");
		}
		catch (Exception e) {
			teststepResult(Status.FAIL, "No Alert Found");
		}
		finally 
		{
			takeSnap();			
		}

	}

	@Override
	public String getAlertText() {
		try {
		String readAlertText=driver.switchTo().alert().getText();
		return readAlertText;
		}
		catch (Exception e) {
			teststepResult(Status.FAIL, "No Alert Found");
		}
		finally 
		{
		 takeSnap();			
		}
		return null;
	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
		teststepResult(Status.PASS, "Windows Closed Successfully");

	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();
		teststepResult(Status.PASS, "All Windows Closed Successfully");

	}

}
