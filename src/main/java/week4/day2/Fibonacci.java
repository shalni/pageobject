package week4.day2;

import java.util.Scanner;

public class Fibonacci {
public static void main(String[] args) 
{int n;
 Scanner s= new Scanner(System.in);
 System.out.println("Enter the number for Fibonacci Series:");
 n=s.nextInt();
 s.close();
 System.out.println("Printing Fibonacci Series: ");
 int i=0;
 int j=1;
 System.out.print(" "+j);
 for(int l=2; l<=n;l++)
 {int t=i+j;
  System.out.print(" "+t);
  i=j;
  j=t;
 }
}
}


