package week4.day2;


import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Shop {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get("https://www.flipkart.com");
		driver.manage().window().maximize();
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		driver.findElementByName("q").sendKeys("iPhone X");
		driver.findElementByClassName("vh79eN").click();
		//System.out.println("Price of second iphone "+driver.findElementByXPath("(//div[@class='_1vC4OE'])[2]").getText());
		List<WebElement> allItemsOff=driver.findElementsByXPath("(//div[@class='_1vC4OE'])"); 
		System.out.println("Discount Price of second iphone "+allItemsOff.get(1).getText());
		//int discPrice=Integer.parseInt(allItemsOff.get(1).getText());
		List<WebElement> allItemsOriginal=driver.findElementsByXPath("//div[@class='_3auQ3N']"); 
		//div[@class='_3auQ3N']
		System.out.println("Actual Price of second iphone "+allItemsOriginal.get(1).getText());
		String originalPrice=allItemsOriginal.get(1).getText();
		originalPrice=originalPrice.replaceAll("\\D", "");
		System.out.println(originalPrice);
		//int originalPrice=Integer.parseInt(allItemsOriginal.get(1).getText());
		//System.out.println("Cost you save is "+ (discPrice-originalPrice));
	}
}
