package week4.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlertFrame {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		driver.switchTo().alert().sendKeys("Shalni");
		driver.switchTo().alert().accept();
		//driver.switchTo().frame("iframeResult");
		//Thread.sleep(30000);
		String message= driver.findElementByXPath("//p[@id='demo']").getText();
		if(message.contains("Shalni"))
			System.out.println("Contains expected String " + message);
		else
			System.out.println("Invalid Message"+ message);
		
	

	}

}
