package week4.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlert {
	public static void main(String[] args) throws InterruptedException {
	System.setProperty("webdriver.chrome.driver", "C:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.get("https://www.irctc.co.in/nget/train-search");
	driver.manage().window().maximize();
	driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
	driver.findElementById("loginbutton").click();
	String alertMessage=driver.switchTo().alert().getText();
	Thread.sleep(3000);
	System.out.println(alertMessage);
	driver.switchTo().alert().accept();
	
	}

}
