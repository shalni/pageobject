package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindows {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
	
			System.setProperty("webdriver.chrome.driver", "C:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver.exe");
			ChromeDriver driver=new ChromeDriver();
			driver.get("https://www.irctc.co.in/nget/train-search");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
			//Thread.sleep(2000);
			driver.findElementByPartialLinkText("Contact Us").click();
			Set<String> allWindows=driver.getWindowHandles();
			List<String> windowsList=new ArrayList<>();
			windowsList.addAll(allWindows);
			driver.switchTo().window(windowsList.get(1));
			System.out.println("Title "+driver.getTitle());
			File src=driver.getScreenshotAs(OutputType.FILE);
			File obj=new File("./screenshots/contacts.jpg");
			FileUtils.copyFile(src, obj);
			driver.switchTo().window(windowsList.get(0)).close();

	}

}
