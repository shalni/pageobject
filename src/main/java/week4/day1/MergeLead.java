package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementByXPath("//input[@id='username']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		driver.findElementByXPath("//a[contains(@href,'crmsfa')]").click();
		driver.findElementByXPath("//a[contains(@href,'leadsMain')]").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();
		Set<String> allWindows=driver.getWindowHandles();
		List<String> windowsList=new ArrayList<>();
		windowsList.addAll(allWindows);
		driver.switchTo().window(windowsList.get(1));
		driver.findElementByXPath("//input[@name='firstName']").sendKeys("Shruthi");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		List<WebElement> allRows=driver.findElementsByXPath("(//table[@class='x-grid3-row-table'])[1]/tbody/tr");
		int rowsCount=allRows.size();
		System.out.println("rowsCount "+rowsCount);
		WebElement eachLeadRow=allRows.get(0);
		List<WebElement> allLeadColumns=eachLeadRow.findElements(By.tagName("a"));
		System.out.println(allLeadColumns.size());
		System.out.println(allLeadColumns.get(0).getText());
		allLeadColumns.get(0).click();
		driver.switchTo().window(windowsList.get(0));
		
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		allWindows=driver.getWindowHandles();
		windowsList=new ArrayList<>();
		windowsList.addAll(allWindows);
		driver.switchTo().window(windowsList.get(1));
		driver.findElementByXPath("//input[@name='firstName']").sendKeys("Shalni");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		allRows=driver.findElementsByXPath("(//table[@class='x-grid3-row-table'])[1]/tbody/tr");
		rowsCount=allRows.size();
		System.out.println("rowsCount "+rowsCount);
		eachLeadRow=allRows.get(0);
		allLeadColumns=eachLeadRow.findElements(By.tagName("a"));
		System.out.println(allLeadColumns.size());
		System.out.println(allLeadColumns.get(0).getText());
		allLeadColumns.get(0).click();
		driver.switchTo().window(windowsList.get(0));
		driver.findElementByLinkText("Merge").click();
		driver.switchTo().alert().accept();
		
	}

}
