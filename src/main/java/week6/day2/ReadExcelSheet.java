package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
public class ReadExcelSheet {
	

	public static Object[][] readExcel() throws IOException
	{
	 XSSFWorkbook wb=new XSSFWorkbook("./data/MySheet.xlsx"); //Go to the Excel Sheet
	 XSSFSheet sheet=wb.getSheetAt(0); //Access the Sheet
	 
	 int rowCount=sheet.getLastRowNum(); //Read the number of Rows
	 //System.out.println("Number Of Rows"+ rowCount);
	 int cellCount=sheet.getRow(0).getLastCellNum();
	 String data[][]=new String[rowCount][cellCount];
	 //System.out.println("Number Of Rows"+ cellCount);
	 for( int i=1;i<=rowCount;i++)
	 { for(int j=0;j<cellCount;j++)
		 { //System.out.print(" "+sheet.getRow(i).getCell(j).getStringCellValue());
		  data[i-1][j]=sheet.getRow(i).getCell(j).getStringCellValue();
		 }
	 }
	 wb.close();
	 return data;
	
	}
}
